----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09.04.2016 11:46:45
-- Design Name: 
-- Module Name: aes_core - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity aes_core is
    Port ( key : in STD_LOGIC_VECTOR (127 downto 0);
           load_key : in STD_LOGIC;
           encrypt : in STD_LOGIC;
           key_ready : out STD_LOGIC;
           clk : in STD_LOGIC;
           reset_n : in STD_LOGIC;
           datavalid_in : in STD_LOGIC;
           data_in : in STD_LOGIC_VECTOR (127 downto 0);
           datavalid_out : out STD_LOGIC;
           data_out : out STD_LOGIC_VECTOR (127 downto 0));
end aes_core;

architecture Behavioral of aes_core is

begin

--Loop back to use with dmatest

data_out<=data_in;
datavalid_out<=datavalid_in;

end Behavioral;
