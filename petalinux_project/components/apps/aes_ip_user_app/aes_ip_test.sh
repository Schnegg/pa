# Copyright (c) 2016, Alexandre Schnegg
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

modprobe xilinx-dma
modprobe dma_proxy

testNumber=1000

for i in $(seq 1 $testNumber)
do
	echo "TEST NUMBER: "$i
	blockNumber=$(shuf -i1-8191 -n1)
	key=$(hexdump -n 16 -v -e '/1 "%02X"' /dev/urandom)
	echo "Key: "$key
	dd if=/dev/urandom of=input  bs=1024  count=$blockNumber >/dev/null
	/bin/aes_ip_user_app input output $key enc >/dev/null
	openssl aes-128-ecb -d -K $key -nosalt -in output -out input.dec >/dev/null
	if cmp input input.dec
	then
		echo "Encryption ok with key: "$key
	else
		echo "Encryption NOT ok with key: "$key
		exit
	fi

	key=$(hexdump -n 16 -v -e '/1 "%02X"' /dev/urandom)
	echo "Key: "$key
	openssl aes-128-ecb -K $key -nosalt -in input -out input.enc >/dev/null
	/bin/aes_ip_user_app input.enc output.dec $key dec >/dev/null
	if cmp input output.dec
	then
		echo "Decryption ok with key: "$key
	else
		echo "Decryption NOT ok with key: "$key
		exit
	fi
done
